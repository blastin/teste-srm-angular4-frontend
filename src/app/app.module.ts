import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ClienteModule} from './cliente/cliente.module';
import {RouterModule, Routes} from '@angular/router';
import {CadastroComponent} from './cliente/cadastro/cadastro.component';
import {ClientesComponent} from './cliente/clientes/clientes.component';
import { InformacaoComponent } from './informacao/informacao.component';

const appRoutes: Routes = [
  {path: 'cadastrar', component: CadastroComponent},
  {
    path: 'clientes',
    component: ClientesComponent,
    data: {title: 'Lista de Clientes'}
  },
  {
    path: '',
    redirectTo: 'informacao',
    pathMatch: 'full'
  },
  {
    path: 'informacao',
    component: InformacaoComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    InformacaoComponent,
  ],
  imports: [
    BrowserModule,
    ClienteModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
