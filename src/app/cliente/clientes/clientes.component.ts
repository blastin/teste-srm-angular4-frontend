import {Component, OnInit} from '@angular/core';
import {PersistenceService} from '../persistence.service';
import {Cliente} from '../cliente';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'Nome Completo', 'Risco', 'Limite de Crédito', 'Taxa de Juros'];
  dataSource: Cliente[];

  constructor(private _persistenceService: PersistenceService) {
  }

  ngOnInit() {
    this.obterClientes();
  }

  obterClientes() {
    this._persistenceService.obterClientes().subscribe(response => this.dataSource = response);
  }

}
