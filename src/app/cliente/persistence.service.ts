import {Injectable} from '@angular/core';
import {Cliente} from './cliente';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PersistenceService {

  private _Url = 'http://127.0.0.1:8080/api/';

    constructor(private http: HttpClient) {
  }

  cadastrarCliente(cliente: Cliente): Observable<any> {
    return this.http.post(this._Url + 'cliente/cadastrar', cliente);
  }

  obterRiscos(): Observable<any> {
    return this.http.get(this._Url + 'risco');
  }

  obterClientes(): Observable<any> {
    return this.http.get(this._Url + 'cliente');
  }
}

