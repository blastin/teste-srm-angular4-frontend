import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CadastroComponent} from './cadastro/cadastro.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {CURRENCY_MASK_CONFIG, CurrencyMaskConfig} from 'ng2-currency-mask/src/currency-mask.config';
import {ClientesComponent} from './clientes/clientes.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule,} from '@angular/material';
import {CurrencyFormatPipe} from './currency-format.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: false,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.'
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CurrencyMaskModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
    declarations: [CadastroComponent, ClientesComponent, CurrencyFormatPipe],
  exports: [CadastroComponent],
  providers: [{provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig}]
})
export class ClienteModule {
}
