import {Credito} from './credito';
import {Risco} from './risco.enum';

export class Cliente {

  id: number;
  nomeCompleto: string;
  credito: Credito;
  risco: Risco;

  constructor() {
    this.nome = '';
  }

  public set nome(nomeCompleto: string) {
    this.nomeCompleto = nomeCompleto;
  }

  public get nome(): string {
    return this.nomeCompleto;
  }

}

