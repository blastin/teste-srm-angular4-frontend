import {Component, OnInit} from '@angular/core';
import {Credito} from '../credito';
import {Cliente} from '../cliente';
import {PersistenceService} from '../persistence.service';
import {Risco} from '../risco.enum';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})

export class CadastroComponent implements OnInit {

  public cliente: Cliente;
  public credito: Credito;
  public riscos: Risco[];
  public alertCadastroRealizado: boolean;
  public alertNaoFoiPossivelCadastro: boolean;

  constructor(private _persistenceService: PersistenceService) {
    this.cliente = new Cliente();
    this.credito = new Credito();
      this.cliente.credito = this.credito;
    this.alertCadastroRealizado = false;
    this.alertNaoFoiPossivelCadastro = false;
  }

  ngOnInit() {
    this.obterRiscos();
  }

  cadastrar() {
    this._persistenceService.cadastrarCliente(this.cliente).subscribe(status => this.validarStatusCadastroCliente(status));
  }

  obterRiscos() {
    this._persistenceService.obterRiscos().subscribe(response => this.riscos = response);
  }

  validarStatusCadastroCliente(status: number): void {
    switch (status) {
      case 201:
        this.alertCadastroRealizado = true;
        break;
      default:
        this.alertNaoFoiPossivelCadastro = true;
        break;
    }
  }
}
